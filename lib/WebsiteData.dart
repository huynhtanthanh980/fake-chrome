import 'package:fake_chrome/ScreenSize.dart';
import 'package:flutter/material.dart';

class WebsiteData {
  String name;
  Widget build;

  WebsiteData(this.name, this.build);
}

List<WebsiteData> websiteList = [
  new WebsiteData(
    "google.com",
    Container(
      height: ScreenSize.safeBlockVerticalSize * 100,
      width: ScreenSize.safeBlockHorizontalSize * 100,
      color: Colors.red,
    ),
  ),
  new WebsiteData(
    "zing.vn",
    Container(
      height: ScreenSize.safeBlockVerticalSize * 100,
      width: ScreenSize.safeBlockHorizontalSize * 100,
      color: Colors.blue,
    ),
  ),
  new WebsiteData(
    "facebook.com",
    Container(
      height: ScreenSize.safeBlockVerticalSize * 100,
      width: ScreenSize.safeBlockHorizontalSize * 100,
      color: Colors.amber,
    ),
  ),
  new WebsiteData(
    "messenger.com",
    Container(
      height: ScreenSize.safeBlockVerticalSize * 100,
      width: ScreenSize.safeBlockHorizontalSize * 100,
      color: Colors.purple,
    ),
  ),
];
