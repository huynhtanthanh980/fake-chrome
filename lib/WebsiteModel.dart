import 'dart:async';

import 'package:fake_chrome/WebsiteData.dart';
import 'package:flutter/cupertino.dart';
import 'package:rxdart/rxdart.dart';

class WebsiteController {
  int _currentWebsiteIndex = -1;

  Sink<int> get changeWebsite => _websiteController.sink;
  final _websiteController = StreamController<int>();

  Stream<Widget> get getWebsiteBuild => _getWebsiteBuildSubject.stream;
  final _getWebsiteBuildSubject = BehaviorSubject<Widget>();

  Stream<String> get getWebsiteURL => _getWebsiteURLSubject.stream;
  final _getWebsiteURLSubject = BehaviorSubject<String>();

  WebsiteController() {
    _websiteController.stream.listen(_buildNewWebsite);
  }

  void _buildNewWebsite(int websiteIndex) {
    if (_currentWebsiteIndex != websiteIndex) {
      if (websiteIndex >= 0) {
        _getWebsiteBuildSubject.add(websiteList[websiteIndex].build);
        _getWebsiteURLSubject.add(websiteList[websiteIndex].name);
        _currentWebsiteIndex = websiteIndex;
      }
      else {
        _getWebsiteBuildSubject.add(null);
        _getWebsiteURLSubject.add("Home");
        _currentWebsiteIndex = -1;
      }
    }
  }

  dispose() {
    _websiteController.close();
    _getWebsiteBuildSubject.close();
    _getWebsiteURLSubject.close();
  }
}