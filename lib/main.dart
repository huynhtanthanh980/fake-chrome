import 'package:fake_chrome/BrowserHomeScreen.dart';
import 'package:fake_chrome/ScreenSize.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SizedClass(),
    );
  }
}

class SizedClass extends StatelessWidget {

  // ignore: missing_return
  Future<int> whenNotZero(Stream<double> source) async {
    await for (double value in source) {
      if (value > 0) {
        return 1; //if screen size is found, return 1
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: whenNotZero(Stream<double>.periodic(Duration(milliseconds: 50),
              (x) => MediaQuery.of(context).size.width)),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          ScreenSize().init(context);
          return BrowserHomeScreen();
        }
        return CircularProgressIndicator();
      },
    );
  }
}