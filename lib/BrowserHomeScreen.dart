import 'package:fake_chrome/CustomColor.dart';
import 'package:fake_chrome/ScreenSize.dart';
import 'package:fake_chrome/WebsiteData.dart';
import 'package:fake_chrome/WebsiteModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BrowserHomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return BrowserHomeScreenState();
  }
}

class BrowserHomeScreenState extends State<BrowserHomeScreen> {
  WebsiteController websiteController = WebsiteController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              buildURLBar(),
            ];
          },
          body: buildBody(),
        ),
      ),
    );
  }

  Widget buildURLBar() {
    return SliverAppBar(
      expandedHeight: ScreenSize.safeBlockVerticalSize * 7,
      backgroundColor: Colors.white,
      forceElevated: true,
      elevation: 10.0,
      floating: true,
      snap: true,
      pinned: false,
      flexibleSpace: FlexibleSpaceBar(
        background: Container(
          height: ScreenSize.safeBlockVerticalSize * 7,
          width: ScreenSize.safeBlockHorizontalSize * 100,
          padding: EdgeInsets.symmetric(
              horizontal: ScreenSize.safeBlockHorizontalSize * 1),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: ScreenSize.safeBlockHorizontalSize * 10,
                width: ScreenSize.safeBlockHorizontalSize * 10,
                child: MaterialButton(
                  padding: EdgeInsets.zero,
                  shape: CircleBorder(),
                  child: Icon(
                    Icons.home,
                    size: ScreenSize.safeBlockHorizontalSize * 6,
                  ),
                  onPressed: () {
                    websiteController.changeWebsite.add(-1);
                  },
                ),
              ),
              SizedBox(
                width: ScreenSize.safeBlockHorizontalSize,
              ),
              Container(
                height: ScreenSize.safeBlockHorizontalSize * 8,
                width: ScreenSize.safeBlockHorizontalSize * 60,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25.0),
                  color: CustomColor.urlBar,
                ),
                child: MaterialButton(
                  padding: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0)),
                  child: Row(
                    children: <Widget>[
                      Container(
                          height: ScreenSize.safeBlockHorizontalSize * 8,
                          width: ScreenSize.safeBlockHorizontalSize * 10,
                          child: Center(
                            child: Icon(
                              Icons.lock,
                              size: ScreenSize.safeBlockHorizontalSize * 5,
                            ),
                          )),
                      Container(
                        height: ScreenSize.safeBlockHorizontalSize * 8,
                        width: ScreenSize.safeBlockHorizontalSize * 40,
                        child: Center(
                          child: StreamBuilder(
                            stream: websiteController.getWebsiteURL,
                            initialData: "Home",
                            builder: (context, snapshot) {
                              return Text(
                                snapshot.data,
                                style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize:
                                      ScreenSize.safeBlockHorizontalSize * 4,
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                      Container(
                        height: ScreenSize.safeBlockHorizontalSize * 8,
                        width: ScreenSize.safeBlockHorizontalSize * 10,
                      ),
                    ],
                  ),
                  onPressed: () {
                    showWebsiteList();
                  },
                ),
              ),
              SizedBox(
                width: ScreenSize.safeBlockHorizontalSize,
              ),
              Container(
                height: ScreenSize.safeBlockHorizontalSize * 10,
                width: ScreenSize.safeBlockHorizontalSize * 10,
                child: MaterialButton(
                  padding: EdgeInsets.zero,
                  shape: CircleBorder(),
                  child: Icon(
                    Icons.history,
                    size: ScreenSize.safeBlockHorizontalSize * 6,
                  ),
                  onPressed: () {},
                ),
              ),
              Container(
                height: ScreenSize.safeBlockHorizontalSize * 10,
                width: ScreenSize.safeBlockHorizontalSize * 10,
                child: MaterialButton(
                  padding: EdgeInsets.zero,
                  shape: CircleBorder(),
                  child: Icon(
                    Icons.more_vert,
                    size: ScreenSize.safeBlockHorizontalSize * 6,
                  ),
                  onPressed: () {},
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildBody() {
    return StreamBuilder(
      stream: websiteController.getWebsiteBuild,
      initialData: buildHomePage(),
      builder: (context, snapshot) {
        if (snapshot.hasData && snapshot.data != null) {
          return snapshot.data;
        }
        return buildHomePage();
      },
    );
  }

  Widget buildHomePage() {
    return Container(
      height: ScreenSize.safeBlockVerticalSize * 100,
      width: ScreenSize.safeBlockHorizontalSize * 100,
      color: Colors.pink,
    );
  }

  void showWebsiteList() async {
    return await showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        return Container(
          width: ScreenSize.safeBlockHorizontalSize * 100,
          padding: EdgeInsets.symmetric(
              vertical: ScreenSize.verticalBlockSize * 3,
              horizontal: ScreenSize.safeBlockHorizontalSize * 5),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(25.0),
                topLeft: Radius.circular(25.0)),
            color: Colors.white,
          ),
          child: ListView.builder(
            itemCount: websiteList.length + 1,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return (index != websiteList.length)
                  ? Padding(
                      padding: EdgeInsets.only(
                          bottom: ScreenSize.verticalBlockSize * 3),
                      child: Container(
                        height: ScreenSize.verticalBlockSize * 6,
                        width: ScreenSize.safeBlockHorizontalSize * 90,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25.0),
                          color: CustomColor.urlBar,
                        ),
                        child: MaterialButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25.0),
                          ),
                          padding: EdgeInsets.zero,
                          child: Center(
                            child: Text(
                              websiteList[index].name,
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize:
                                    ScreenSize.safeBlockHorizontalSize * 4,
                              ),
                            ),
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                            websiteController.changeWebsite.add(index);
                          },
                        ),
                      ),
                    )
                  : Padding(
                      padding: EdgeInsets.only(
                          top: ScreenSize.verticalBlockSize * 3),
                      child: Container(
                        height: ScreenSize.verticalBlockSize * 6,
                        width: ScreenSize.safeBlockHorizontalSize * 90,
                        padding: EdgeInsets.symmetric(
                            vertical: ScreenSize.verticalBlockSize * 1,
                            horizontal: ScreenSize.safeBlockHorizontalSize * 5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25.0),
                          color: Colors.grey,
                        ),
                        child: MaterialButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25.0),
                          ),
                          child: Center(
                            child: Text(
                              "Cancel",
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize:
                                    ScreenSize.safeBlockHorizontalSize * 4,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    );
            },
          ),
        );
      },
    );
  }
}
